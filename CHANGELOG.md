- 0.1-Beta
    - Primera subida de clases con la funcionalidad aun sin testear

- 0.2-Beta
    - Se corrige error de nombramiento de paquetes
    - Se agregan funcionalidades en clase Board y Cell
    - Falta corregir algoritmo principal.

- 0.3-Beta
    - Se corrigen errores al validar celulas
    - Se crea clase Main para maniobrar en consola
