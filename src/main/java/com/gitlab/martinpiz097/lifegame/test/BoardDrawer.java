package com.gitlab.martinpiz097.lifegame.test;

import com.gitlab.martinpiz097.lifegame.core.Board;

import java.util.LinkedList;
import java.util.Scanner;

public class BoardDrawer extends Thread {
    private final Scanner scanner;
    private volatile Board board;

    public BoardDrawer() {
        scanner = new Scanner(System.in);
    }

    private void clearConsole() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    @Override
    public void run() {
        int rows = 0;
        int columns = 0;

        while (rows == 0 && columns == 0) {
            try {
                clearConsole();
                System.out.print("Rows: ");
                rows = scanner.nextInt();
                System.out.print("Columns: ");
                columns = scanner.nextInt();
                if (rows < 0 || columns < 0) {
                    rows = 0;
                    columns = 0;
                }

            } catch (NumberFormatException e) {
                rows = 0;
                columns = 0;
            }
        }
        board = new Board(rows, columns);
        clearConsole();

        int cellsCount = 0;
        while (cellsCount == 0) {
            try {
                clearConsole();
                System.out.print("Cells to add? ");
                cellsCount = scanner.nextInt();
                if (cellsCount < 0 || cellsCount > board.getCapcity())
                    cellsCount = 0;
            } catch (NumberFormatException e) {
                cellsCount = 0;
            }
        }
        clearConsole();
        int row = -1;
        int column = -1;

        for (int i = 0; i < cellsCount; i++) {
            while (row == -1 && column == -1) {
                try {
                    clearConsole();
                    System.out.print("Cell Row: ");
                    row = scanner.nextInt();
                    System.out.print("Cell Column: ");
                    column = scanner.nextInt();

                    if ((row > board.getRowLimit() ||
                            column > board.getColumnLimit())
                            || (row < 0 || column < 0)) {
                        row = -1;
                        column = -1;
                    }
                } catch (NumberFormatException e) {
                    row = -1;
                    column = -1;
                }
            }
            board.reviveCell(row, column);
            row = -1;
            column = -1;
        }

        long interval = 0;
        while (interval == 0) {
            try {
                    clearConsole();
                System.out.print("Interval(ms): ");
                interval = scanner.nextInt();
                if (interval < 0)
                    interval = 0;
            } catch (NumberFormatException e) {
                interval = 0;
            }
        }

        while (true) {
            clearConsole();
            System.out.println(board.toString());
            board.advanceStage();
            try {
                Thread.sleep(interval);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }
}
