package com.gitlab.martinpiz097.lifegame.test;

import com.gitlab.martinpiz097.lifegame.core.Board;
import com.gitlab.martinpiz097.lifegame.core.Cell;

import java.io.IOException;
import java.io.InputStream;

public class BoardManager extends Thread {
    private Board board;

    public BoardManager() {
        this.board = new Board();
        board.reviveCell(4, 5);
        board.reviveCell(4, 6);
        board.reviveCell(4, 7);
    }

    private void clearConsole() {
        /*System.out.print("\033[H\033[2J");
        System.out.flush();*/
        try {
            Process clear = Runtime.getRuntime().exec("clear");
            clear.waitFor();
            InputStream inputStream = clear.getInputStream();
            int read = 0;
            while ((read = inputStream.read()) != -1)
                System.out.print((char)read);
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }

    }

    private void printCells() {
        Cell[][] cells = board.getCells();
        StringBuilder sbBoard = new StringBuilder();

        char deadCell = (char) 176;
        char aliveCell = (char) 178;

        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[0].length; j++) {
                sbBoard.append(cells[i][j].isAlive()?aliveCell:deadCell)
                        .append(' ');
            }
            sbBoard.deleteCharAt(sbBoard.length()-1);
            sbBoard.append('\n');
        }
        System.out.print(sbBoard.toString());
    }

    @Override
    public void run() {
        while (true) {
            printCells();
            board.advanceStage();
            clearConsole();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    /*
    public static void main(String[] args) {
        BoardManager manager = new BoardManager();
        manager.start();
    }
    */


}
