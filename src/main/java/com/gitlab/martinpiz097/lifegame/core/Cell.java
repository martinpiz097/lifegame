package com.gitlab.martinpiz097.lifegame.core;

public class Cell {
    private boolean alive;
    //private Position position;
    //private Board universe;

    public Cell() {
        this(false);
    }

    public Cell(boolean alive/*, Position position, Board universe*/) {
        this.alive = alive;
        //this.position = position;
        //this.universe = universe;
    }

    /*public Cell(Position position, Board universe) {
        this(false, position, universe);
    }

    public Cell(boolean alive, int row, int column, Board universe) {
        this(alive, new Position(row, column), universe);
    }*/

    public boolean isAlive() {
        return alive;
    }

    public String getState() {
        return alive?"Alive":"Dead";
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public void setAlive(int liveAround) {
        boolean vive = false;
        boolean aliv = alive;
        if (alive) {
            if (liveAround < 2 || liveAround > 3)
                kill();
            else
                vive = true;
        }
        else if (liveAround == 3) {
            vive = true;
            revive();
        }
        /*System.out.println("Celula con estado "+aliv+": "+
                (vive?"Vive":"Muere")+" al tener " +
                        ""+liveAround+" celulas alrededor");
                        */
    }

    public void revive() {
        alive = true;
    }

    public void kill() {
        alive = false;
    }

    @Override
    public String toString() {
        return "Cell:"+getState();
    }
}
