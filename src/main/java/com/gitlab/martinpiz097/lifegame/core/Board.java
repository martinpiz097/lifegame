package com.gitlab.martinpiz097.lifegame.core;

import com.gitlab.martinpiz097.lifegame.sys.ConsoleColor;

import java.util.Arrays;

public class Board {
    private Cell[][] cells;

    public Board() {
        this(10, 10);
    }

    public Board(int rows, int columns) {
        cells = new Cell[rows][columns];
        buildDefaltCells();
    }

    private void buildDefaltCells() {
        for (int i = 0; i < getRowCount(); i++)
            for (int j = 0; j < getColumnCount(); j++)
                cells[i][j] = new Cell();
    }

    private Cell[][] getCellsCopy() {
        int rows = getRowCount();
        int columns = getColumnCount();
        Cell[][] copy = new Cell[rows][columns];
        for (int i = 0; i < getRowCount(); i++) {
            for (int j = 0; j < getColumnCount(); j++) {
                copy[i][j] = new Cell(cells[i][j].isAlive());
            }
        }
        return copy;
    }

    private int getLiveAround(int row, int column, Cell[][] cells) {
        int liveAround = 0;
        int rowLimit = getRowLimit();
        int columnLimit = getColumnLimit();

        if ((row == 0 || column == 0) ||
                (row == rowLimit || column == columnLimit)) {
            if (row == 0 && column == 0) {
                if (cells[row + 1][column].isAlive())
                    liveAround++;
                if (cells[row + 1][column + 1].isAlive())
                    liveAround++;
                if (cells[row][column + 1].isAlive())
                    liveAround++;
            } else if (row == 0 && column == columnLimit) {
                if (cells[row + 1][column].isAlive())
                    liveAround++;
                if (cells[row][column - 1].isAlive())
                    liveAround++;
                if (cells[row + 1][column - 1].isAlive())
                    liveAround++;
            } else if (row == rowLimit && column == 0) {
                if (cells[row - 1][column].isAlive())
                    liveAround++;
                if (cells[row - 1][column + 1].isAlive())
                    liveAround++;
                if (cells[row][column + 1].isAlive())
                    liveAround++;
            } else if (row == rowLimit && column == columnLimit) {
                if (cells[row - 1][column].isAlive())
                    liveAround++;
                if (cells[row - 1][column - 1].isAlive())
                    liveAround++;
                if (cells[row][column - 1].isAlive())
                    liveAround++;
            } else if (row == 0) {
                if (cells[row][column - 1].isAlive())
                    liveAround++;
                if (cells[row][column + 1].isAlive())
                    liveAround++;
                if (cells[row + 1][column - 1].isAlive())
                    liveAround++;
                if (cells[row + 1][column].isAlive())
                    liveAround++;
                if (cells[row + 1][column + 1].isAlive())
                    liveAround++;
            } else if (column == 0) {
                if (cells[row - 1][column].isAlive())
                    liveAround++;
                if (cells[row + 1][column].isAlive())
                    liveAround++;
                if (cells[row - 1][column + 1].isAlive())
                    liveAround++;
                if (cells[row][column + 1].isAlive())
                    liveAround++;
                if (cells[row + 1][column + 1].isAlive())
                    liveAround++;
            } else if (row == rowLimit) {
                if (cells[row][column - 1].isAlive())
                    liveAround++;
                if (cells[row][column + 1].isAlive())
                    liveAround++;
                if (cells[row - 1][column - 1].isAlive())
                    liveAround++;
                if (cells[row - 1][column].isAlive())
                    liveAround++;
                if (cells[row - 1][column + 1].isAlive())
                    liveAround++;
            } else if (column == columnLimit) {
                if (cells[row - 1][column].isAlive())
                    liveAround++;
                if (cells[row + 1][column].isAlive())
                    liveAround++;
                if (cells[row - 1][column - 1].isAlive())
                    liveAround++;
                if (cells[row][column - 1].isAlive())
                    liveAround++;
                if (cells[row + 1][column - 1].isAlive())
                    liveAround++;
            }

        } else {
            if (cells[row][column - 1].isAlive())
                liveAround++;
            if (cells[row][column + 1].isAlive())
                liveAround++;
            if (cells[row - 1][column - 1].isAlive())
                liveAround++;
            if (cells[row - 1][column].isAlive())
                liveAround++;
            if (cells[row - 1][column + 1].isAlive())
                liveAround++;
            if (cells[row + 1][column - 1].isAlive())
                liveAround++;
            if (cells[row + 1][column].isAlive())
                liveAround++;
            if (cells[row + 1][column + 1].isAlive())
                liveAround++;
        }
        return liveAround;
    }

    public void advanceStage() {
        int rows = cells.length;
        int columns = cells[0].length;
        Cell[][] cellsCopy = getCellsCopy();
        int liveAround;
        boolean show = false;

        for (int i = 0; i < rows; i++)
            for (int j = 0; j < columns; j++)
                cells[i][j].setAlive(getLiveAround(i, j, cellsCopy));
        cellsCopy = null;
    }

    public int getCapcity() {
        return getRowCount()*getColumnCount();
    }

    public int getRowCount() {
        return cells.length;
    }

    public int getColumnCount() {
        return cells[0].length;
    }

    public int getRowLimit() {
        return cells.length-1;
    }

    public int getColumnLimit() {
        return cells[0].length-1;
    }

    public Cell[][] getCells() {
        return cells;
    }

    public Cell getCell(int row, int column) {
        return cells[row][column];
    }

    public void setCell(int row, int column, boolean alive) {
        getCell(row, column).setAlive(alive);
    }

    public void reviveCell(int row, int column) {
        getCell(row, column).revive();
    }

    public void killCell(int row, int column) {
        getCell(row, column).kill();
    }

    public void setCells(Cell[][] cells) {
        this.cells = cells;
    }

    public String toString(Cell[][] cells) {
        StringBuilder sbBoard = new StringBuilder();

        Cell cell;
        int columnLimit = getColumnLimit();
        boolean cellAlive;

        for (int i = 0; i < getRowCount(); i++) {
            for (int j = 0; j < getColumnCount(); j++) {
                cell = cells[i][j];
                cellAlive = cell.isAlive();
                sbBoard.append(cellAlive? ConsoleColor.GREEN:ConsoleColor.RED)
                        /*.append(cellAlive?'A':'D')
                        .append('{')
                        .append(i).append(',').append(j)
                        .append("}:")
                        .append(getLiveAround(i, j, cells))*/
                        .append((char)48)
                        .append(ConsoleColor.RESET);
                if (j < columnLimit)
                    sbBoard.append(" - ");
            }
            sbBoard.append('\n');
        }
        sbBoard.deleteCharAt(sbBoard.length()-1);
        return sbBoard.toString();
    }

    @Override
    public String toString() {
        return toString(cells);
    }

}
